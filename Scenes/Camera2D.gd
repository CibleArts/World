extends Camera2D

var zoom_min = Vector2(.200001, .200001)
var zoom_max = Vector2(4,4)
var zoom_speed = Vector2(.2, .2)
var des_zoom = zoom

const SPEED = 500

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	zoom = lerp(zoom, des_zoom, .2)

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				if des_zoom < zoom_max:
					des_zoom += zoom_speed
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				if des_zoom > zoom_min:
					des_zoom -= zoom_speed
				
