extends Camera2D

var zoom_min = Vector2(.200001, .200001)
var zoom_max = Vector2(4,4)
var zoom_speed = Vector2(.2, .2)
var des_zoom = zoom

const MOVE_SPEED = 100

func _process(delta):
	zoom = lerp(zoom, des_zoom, .2)
	if Input.is_action_pressed("ui_left"):
		global_position += Vector2.LEFT * delta * MOVE_SPEED
	elif Input.is_action_pressed("ui_right"):
		global_position += Vector2.RIGHT * delta * MOVE_SPEED


func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				if des_zoom < zoom_max:
					des_zoom += zoom_speed
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				if des_zoom > zoom_min:
					des_zoom -= zoom_speed
