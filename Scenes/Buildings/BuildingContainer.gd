extends FlowContainer

var tower = load("res://Scenes/Buildings/tower.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_tower_button_gui_input(event):
	var tower_temp = tower.instantiate()
	if event is InputEventMouseButton and event.button_mask == 1:
		add_child(tower_temp)
		tower_temp.process_mode = Node.PROCESS_MODE_DISABLED
	elif event is InputEventMouse and event.button_mask == 1:
		get_child(1).global_position = event.global_position
	elif event is InputEventMouseButton and event.button_mask == 0:
		get_parent().get_parent().add_child(tower_temp)
		get_child(1).queue_free()
		tower_temp.global_position = get_parent().get_parent().get_global_mouse_position()
		
